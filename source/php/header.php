<!DOCTYPE html>

<html lang="pt_BR">

<head>
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-KNGDH33');
  </script>
  <!-- End Google Tag Manager -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176156765-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-176156765-1');
  </script>

  <!-- meta facebook -->
  <meta name="facebook-domain-verification" content="a4uknqkkizto67clbxd8g30v69t3dv" />

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>

    Máquinas São Joaquim

  </title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KNGDH33" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <header class="bg-yellow">
    <nav class="navbar navbar-expand-lg navbar-light">
      <a class="navbar-brand d-lg-none" href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="d-lg-none menu-mobile">
          <div class="whats">
            Fale conosco<br>pelo <b>WhatsApp</b><br><a href="https://api.whatsapp.com/send?phone=55<?= the_field('whatsapp', 33) ?>&text=Ol%C3%A1%2C%20vim%20atrav%C3%A9s%20da%20landing%20page%20e%20gostaria%20de%20saber%20mais%20sobre%20os%20seus%20produtos%2C%20poderia%20me%20ajudar%3F" class="whats-link" targe="_blank"><i class="fab fa-whatsapp"></i><? echo(formatarNumeroCelular(get_field('whatsapp', 33))) ?></a>
          </div>
          <a href="#orcamento" class="btn-geral btn-geral-2 bg-blue"><i>PEÇA SEU ORÇAMENTO</i></a>
          <div class="redes">
            <a href="<?= the_field('instagram', 33)?>" target="_blank"><i class="fab fa-instagram"></i></a> <a href="<?= the_field('facebook', 33)?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
          </div>
        </div>
        <div class="d-none d-lg-flex menu-desktop col-lg-7">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">
          <div class="redes">
            <a href="<?= the_field('instagram', 33)?>" target="_blank"><i class="fab fa-instagram"></i></a> <a href="<?= the_field('facebook', 33)?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="https://api.whatsapp.com/send?phone=55<?= the_field('whatsapp', 33) ?>&text=Ol%C3%A1%2C%20vim%20atrav%C3%A9s%20da%20landing%20page%20e%20gostaria%20de%20saber%20mais%20sobre%20os%20seus%20produtos%2C%20poderia%20me%20ajudar%3F" class="btn-whats" targe="_blank"><i class="fab fa-whatsapp"></i><? echo(formatarNumeroCelular(get_field('whatsapp', 33))) ?></a>
          </div>
        </div>
      </div>
    </nav>
  </header>
  <div class="mask-header d-lg-none"></div>
  <section class="bg-blue d-lg-none lead-mobile">
    <div class="container">
      <?php if(is_page('obrigado')): ?>
        <div class="mb-110" style="margin-bottom:110px;">
            <h1>Recebemos o seu contato!<br><small class="my-5 d-block">Em breve um de nossos consultores entrará em contato</small></h1>
            <p>Aproveite para nos seguir nas redes sociais!</p>
            <div class="d-flex col-5 col-lg-3 px-0 m-auto justify-content-between">
                <a class="color-yellow" target="_blank" href="https://www.facebook.com/cafesaojoaquim"><i class="fab fa-2x fa-facebook-f"></i></a> 
                <a target="_blank" href="<?= the_field('instagram', 33)?>" class="color-yellow"><i class="fab fa-2x fa-instagram"></i></a> 
                <!-- <a target="_blank" href="https://www.linkedin.com/company/mosca-log%C3%ADstica/?viewAsMember=true" class="color-blue"><i class="fab fa-2x fa-linkedin"></i></a> -->
            </div>
        </div>
      <?php else: ?>
      <h1 class="titulo">
        <i>Seu negócio precisa de uma</i><br><b>MÁQUINA DE CAFÉ?</b><br><small><i>Locação de máquinas de café <br>automáticas, multiprodutos e <br>profissionais</i></small>
      </h1>
      <div class="form" id="orcamento">
        <?= do_shortcode('[contact-form-7 id="5" title="Leads mobile"]') ?>
      </div>
      <?php endif; ?>
    </div>

  </section>
  <section class="lead-desktop d-none d-lg-flex">
    <div class="col-lg-7">
    <?php if(is_page('obrigado')): ?>
        <div class="container">
            <h1>Recebemos o seu contato!<br><small class="my-5 d-block">Em breve um de nossos consultores entrará em contato</small></h1>
            <p>Aproveite para nos seguir nas redes sociais!</p>
            <div class="d-flex col-5 col-lg-3 px-0 m-auto justify-content-between">
                <a class="color-yellow" target="_blank" href="https://www.facebook.com/cafesaojoaquim"><i class="fab fa-2x fa-facebook-f"></i></a> 
                <a target="_blank" href="<?= the_field('instagram', 33)?>" class="color-yellow"><i class="fab fa-2x fa-instagram"></i></a> 
                <!-- <a target="_blank" href="https://www.linkedin.com/company/mosca-log%C3%ADstica/?viewAsMember=true" class="color-blue"><i class="fab fa-2x fa-linkedin"></i></a> -->
            </div>
        </div>
      <?php else: ?>
      <h1 class="titulo"><i>Seu negócio precisa de uma</i><br><b>MÁQUINA DE CAFÉ?</b><br></h1>
      <small><i>Locação de máquinas de café <br>automáticas, multiprodutos e profissionais</i></small>
      <div class="form" id="orcamento">
        <?= do_shortcode('[contact-form-7 id="6" title="Leads desktop"]') ?>
      </div>
      <?php endif; ?>
    </div>
  </section>